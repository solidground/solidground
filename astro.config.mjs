import { defineConfig } from 'astro/config';
import starlight from '@astrojs/starlight';
import starlightLinksValidator from 'starlight-links-validator';
import tailwind from "@astrojs/tailwind";

// https://astro.build/config
export default defineConfig({
  integrations: [starlightLinksValidator(), starlight({
    title: 'Solidground',
    locales: {
      root: {
        label: 'English',
        lang: 'en',
      },
    },
    social: {
      matrix: 'https://matrix.to/#/#solidground:matrix.org',
      codeberg: 'https://codeberg.org/solidground/'
    },
    defaultLocale: 'en',
    logo: {
      light: '/src/assets/images/solidground-logo-bare.svg',
      dark: '/src/assets/images/solidground-logo-bare.svg',
      replacesTitle: false
    },
    head: [
      {
        tag: 'meta',
        attrs: { property: 'og:image', content: '/og.jpg' },
      },
      {
        tag: 'meta',
        attrs: { property: 'twitter:image', content: '/og.jpg' },
      },
    ],
    customCss: ['./src/assets/styles/tailwind.css'],
    sidebar: [{
      label: 'Guides',
      items: [{
        label: 'Getting started',
        link: '/guides/getting-started/'
      }, {
        label: 'Floorplanner designer',
        link: '/guides/floorplanner-designer/'
      }, {
        label: 'Taskweave engine',
        link: '/guides/taskweave-engine/'
      }, {
        label: 'Groundwork hosting',
        link: '/guides/groundwork-hosting/'
      }]
    }, {
      label: 'Tutorials',
      items: [{
        label: 'Create your social experience',
        link: '/tutorials/create-your-social-experience/'
      }, {
        label: 'Using the Weave SDK',
        link: '/tutorials/using-the-weave-sdk/'
      }]
    }, {
      label: 'Explanation',
      items: [{
        label: 'Social experience design',
        link: '/explanation/social-experience-design/'
      }, {
        label: 'Domain modeling',
        link: '/explanation/domain-modeling/'
      }, {
        label: 'Process design',
        link: '/explanation/process-design/'
      }, {
        label: 'Blueprint scaffolding',
        link: '/explanation/blueprint-scaffolding/'
      }, {
        label: 'Component development',
        link: '/explanation/component-development/'
      }, {
        label: 'Moldable methods',
        link: '/explanation/moldable-methods/'
      }]
    }, {
      label: 'Showcases',
      collapsed: true,
      items: [{
        label: 'Floorplanner SX',
        link: '/showcases/sx-floorplanner-designer/',
        badge: {
          text: 'iterating',
          variant: 'caution'
        }
      }, {
        label: 'Mimic Interop Suite SX',
        link: '/showcases/sx-mimic-interop-testsuite/',
        badge: {
          text: 'later',
          variant: 'tip'
        }
      }]
    }, {
      label: 'Development',
      collapsed: true,
      items: [{
        label: 'Project roadmap',
        link: '/development/project-roadmap/'
      }, {
        label: 'Solidground requirements',
        link: '/development/requirements/'
      }, {
        label: 'Architecture decisions',
        link: '/development/adrs/'
      }, {
        label: 'Contributing',
        link: '/development/contributing/'
      }, {
        label: 'Groundwork labs',
        link: '/development/groundwork-labs/',
        badge: {
          text: 'coming',
          variant: 'tip'
        }
      }]
    }, {
      label: 'Reference',
      collapsed: true,
      items: [{
        label: 'Glossary',
        link: '/reference/glossary/'
      }, {
        label: 'Weave CLI',
        link: '/reference/weave-cli/'
      }, {
        label: 'Weave SDK',
        link: '/reference/weave-sdk/'
      }]
    }]
  }), 
  tailwind({
    // Disable the default base styles:
    applyBaseStyles: false,
  })],
  // Process images with sharp: https://docs.astro.build/en/guides/assets/#using-sharp
  image: {
    service: {
      entrypoint: 'astro/assets/services/sharp'
    }
  }
});