<div align="center">

[![solidground logo](src/assets/images/solidground-logo-white-500x103.png)](https://solidground.work)

### [Website](https://solidground.work) | [Docs](https://docs.solidground.work) | [Chat](https://matrix.to/#/#solidground-matters:matrix.org) | [Forum](https://discuss.coding.social/c/solidground/13)

</div>

<br>

# Solidground Documentation Website

## Project Structure

Solidground uses [Astro](https://docs.astro.build) and the [Starlight](https://starlight.astro.build/) theme. Relevant folders and files are:

```
.
├── public/
├── src/
│   ├── assets/
│   ├── content/
│   │   ├── adrs/
│   │   ├── docs/
│   │   └── config.ts
│   └── env.d.ts
├── astro.config.mjs
├── package.json
└── tsconfig.json
```

Starlight looks for `.md` or `.mdx` files in the `src/content/docs/` directory. Each file is exposed as a route based on its file name.

Images can be added to `src/assets/` and embedded in Markdown with a relative link.

Static assets, like favicons, can be placed in the `public/` directory.

## Commands

All commands are run from the root of the project, from a terminal:

| Command                   | Action                                           |
| :------------------------ | :----------------------------------------------- |
| `npm install`             | Installs dependencies                            |
| `npm run dev`             | Starts local dev server at `localhost:3000`      |
| `npm run build`           | Build your production site to `./dist/`          |
| `npm run preview`         | Preview your build locally, before deploying     |
| `npm run astro ...`       | Run CLI commands like `astro add`, `astro check` |
| `npm run astro -- --help` | Get help using the Astro CLI                     |

Use `build.sh` to publish the website to Codeberg Pages.

## Code of conduct

We practice [Social Coding Principles](https://coding.social/principles). By participating in our
projects you consent to our [Community Participation Guidelines](CODE_OF_CONDUCT.md).

## License

Copyright (c) 2023-present [Solidground Project](https://solidground.work) and contributors.

This repository is licensed under [AGPL-3.0](LICENSE).