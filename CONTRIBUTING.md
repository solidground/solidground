# Contributing to Solidground

Solidground is a community-led project that is part of the [Social Coding Movement](https://coding.social). We aim to offer an inclusive project environment that allows people with very different backgrounds and skill set to be able to participate and help make our software better. All interaction with our project is subject to the [Social Coding Community Participation Guidelines](https://participate.coding.social).

## How you can contribute

[Social experience design](https://solidground.work/explanation/social-experience-design) involves anticipating the needs of ALL stakeholders. That includes the various roles of people working on Solidground itself. We have documented a number of ways that you as Solidground stakeholder can contribute.

- [Any stakeholder](https://solidground.workdevelopment/contributing/#your-feedback-counts): Your feedback counts. How can we improve our project, our process and our solutions?
- [Advocate](https://solidground.work/development/contributing/#help-spread-the-word): Help spread the word. Communicate about the work we do, and encourage more people to join.
- [Technical writer](https://solidground.work/development/contributing/#improve-the-docs): Improve the docs, fix typo's and grammar and help ensure texts are always up-to-date.
- [Tester](https://solidground.work/development/contributing/#test-drive-our-solutions): Write unit, integration and behavior tests. Improve Gherkin test scripts.
- [UX Designer](https://solidground.work/development/contributing/#design-the-experience): Sketch user interface widgets, help with storyboarding and interaction design.
- [Developer](https://solidground.work/development/contributing/#develop-the-code): Write code for front and back-end of our tools, help fix bugs and implement new features.
- [Researcher](https://solidground.workdevelopment/contributing/#research-our-future): Help put your knowledge to practice and guide us to the Next Generation Internet.

## Contributing to the codebase

