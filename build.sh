#!/usr/bin/env bash
readonly PROJECT_ROOT=$(pwd)
readonly DIST=dist

cd $PROJECT_ROOT
original_branch=$(git branch --show-current)
tmp_dir=$(mktemp -d)

npm install
npm run build

cp -r $DIST/* $tmp_dir
cp .domains $tmp_dir

if [[ -z $(git ls-remote --heads origin pages) ]]
then
    echo "[*] Creating deployment branch pages"
    git checkout --orphan pages || true
	git switch pages
else
    echo "[*] Deployment branch pages exists, pulling changes from remote"
    git fetch origin pages
    git switch pages
fi

git rm -rf .
/bin/rm -rf *
cp -r $tmp_dir/* .
cp -r $tmp_dir/.domains .
git add --all
if [ $(git status --porcelain | xargs | sed '/^$/d' | wc -l) -gt 0 ];
then
    echo "[*] Repository has changed, committing changes"
    git commit \
        --message="new deploy: $(date --iso-8601=seconds)"
fi
git checkout $original_branch
git push origin pages --force
