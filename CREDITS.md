# Credits

Free Software is built on the shoulders of giants. In our project we use numerous dependencies to other FOSS projects, and we thank dearly all the many people that helped make Solidground possible by their work.

In our codebase we have inlined and derived works directly from other projects, and these are listed below:


- `Starlight` theme from [Astro](https://starlight.astro.build), MIT.
- `RFC issue template` from [wasmCloud](https://wasmcloud.com), Apache 2.0.
- `TimelineComponent` from [Athena OS](https://athenaos.org/), GPL-3.0
- `FooterCtaSection` from [Tailwind Kit](https://www.tailwind-kit.com/components/cta), MIT.