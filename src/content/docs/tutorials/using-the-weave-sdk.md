---
title: Using the Weave SDK
description: Learn how to set up and use the Weave SDK to develop a service component.
---

:::tip[Help improve the Docs]
Solidground is in early stages of development. Any [contribution](https://codeberg.org/solidground/housekeeping/src/branch/main/CONTRIBUTING.md) is welcome.
:::

The Weave software development kit helps make development of service components easy. The SDK allows creators to bootstrap project structure and generate boilerplate code from templates as well as metadata defined in the solution Blueprint as it evolves. The SDK currently supports development in Rust and Golang. See the [Weave SDK Reference](/reference/weave-sdk/) for installation procedures and technical background.