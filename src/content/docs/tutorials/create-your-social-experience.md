---
title: Create your social experience
description: Use this step-by-step tutorial and design your own Solidground social experience.
---

:::tip[Help improve the Docs]
Solidground is in early stages of development. Any [contribution](https://codeberg.org/solidground/housekeeping/src/branch/main/CONTRIBUTING.md) is welcome.
:::