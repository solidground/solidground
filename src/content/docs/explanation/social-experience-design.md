---
title: Social Experience Design
description: An introduction to Social experience design (SX) and how it relates to Solidground.
---

:::tip[Help improve the Docs]
Solidground is in early stages of development. Any [contribution](https://codeberg.org/solidground/housekeeping/src/branch/main/CONTRIBUTING.md) is welcome.
:::

Social experience design offered by Solidground is an inclusive process to software development that captures the lifecycle of a solution from inception, through maintenance and extension, up to an eventual phasing out of the delivered work.

In 2009, Christian Crumlish published [The Information Architecture of Social Experience Design](https://asistdl.onlinelibrary.wiley.com/doi/epdf/10.1002/bult.2009.1720350605) ([archive link](https://web.archive.org/web/20231020101959/https://asistdl.onlinelibrary.wiley.com/doi/epdf/10.1002/bult.2009.1720350605)), in preparation of a book called [Designing Social Interfaces](https://www.oreilly.com/library/view/designing-social-interfaces/9781491919842/) with Erine Malone and an accompanying [Pattern Library](https://web.archive.org/web/20120512110712/http://www.designingsocialinterfaces.com/patterns/Main_Page).

Then in 2012 Toshihiko Yamakami, in the paper [From User Experience to Social Experience](https://doi.ieeecomputersociety.org/10.1109/UIC-ATC.2012.142), coined Social experience design as a separate field distinct from User experience design (UX). Toshihiko's research went further into their relationship, and in [another paper](https://link.springer.com/chapter/10.1007/978-3-642-41674-3_6) he defined SX as:

> _Design of _the way a person feels about other humans_ through a computer-user interface._

Solidground gives broader meaning to SX than what is expressed in this definition. A social experience doesn't stand on its own, but must fit in with its broader surroundings, both online and offline. Furthermore solutions that offer a social experience evolve over time, and this evolution is part of the experience itself. In alignment with [Social Coding Movement](https://coding.social) the Solidground project considers the full Free Software Development Lifecycle (FSDL). Our definition reads as follows:

:::note[Social experience design]
Design of _the way people socially interact with other humans_ throughout the entire lifecycle and evolution of Social Web solutions.
:::

This definition makes clear how Social experience design can be an enrichment to UX design, which is more focused on how a single person feels when using software solutions. SX involves adopting a mindset where people's social interactions are front and center.

:::note[Social network]
_Any online system_ where people directly or indirectly interact with each other.
:::

Solidground considers any direct or indirect online interactions between people to involve social networking, for which a social experience can be tailored as the solution that satisfies the client.

## Focus on needs

Solidground facilitates close collaboration between clients and creators. Both are stakeholder groups where many people in different roles can be involved. Every stakeholder has specific needs to be satisfied by the solution. In all lifecycle stages the discovered needs drive the design process that is followed to further evolve the solution.

:::tip[Advantage of needs-first approach]
The emphasis on stakeholder needs keeps focus on building the right things and a more inclusive development process. Many FOSS projects build functionality without due consideration how it fits best into the larger context of the client's domain. Over time such projects may suffer from growing complexity, feature bloat, and rigidity to change.
:::

The approach that Solidground takes to discovery of needs is based on [Domain Driven Design](/explanation/domain-modeling) (DDD). The Floorplanner experience designer allows the creator to elaborate the domain models of the solution to fit in with the work methods of the client. The client is the domain expert.

With Floorplanner the client's needs are tracked all the way through their implementation in code, up to the social experience that stakeholders interact with. The [Floorplanner designer](/guides/floorplanner-designer) guide delves into more detail on how this works.

## Focus on process

Solution design follows processes that are typically different for any creator and client, and set up according to individual preferences to align with work methods and organization structure. Out of the box Solidground offers an opinionated development method that is designed to get people started with ease.

<img src="/images/solidground-solution-design-process_excalidraw-dark.png" class="dark-only"
  alt="Diagram that shows the high-level solution design process that is configured by default. In a continuous cycle five steps are iterated through: 1. Needs discovery, and 2. Domain modeling, where the client is in the lead, followed into implementation of 3. Process design, 4. Blueprint scaffolding, and 5. Component development. Then with evolution the cycle repeats."/>
<img src="/images/solidground-solution-design-process_excalidraw-light.png" class="light-only"
  alt="Diagram that shows the high-level solution design process that is configured by default. In a continuous cycle five steps are iterated through: 1. Needs discovery, and 2. Domain modeling, where the client is in the lead, followed into implementation of 3. Process design, 4. Blueprint scaffolding, and 5. Component development. Then with evolution the cycle repeats."/>

Main goal is to deeply involve the client in the design process, to gradually evolve a solution, that truly satisfies the client's needs. And deliver a social experience that serves the stakeholders in their online interaction. At a high level development involves [Strategic Design](/explanation/domain-modeling/#strategic-design) in five stages:

1. [Needs discovery](/explanation/domain-modeling/#discover-needs): Focus on what is most important to deliver a good solution.
2. [Domain modeling](/explanation/domain-modeling/#bounded-contexts): Explore how the solution fits in its surrounding environment.
3. [Process design](/explanation/process-design): Investigate how stakeholders interact in an integral social experience.
4. [Blueprint scaffolding](/explanation/blueprint-scaffolding): Help bootstrap the codebase and guide through implementation tasks.
5. [Component development](/explanation/component-development): Develop the business logic and UI's of service components.

Client and creator collaborate using Floorplanner to ensure they have a common understanding on the status of the ongoing work at all times and are able to collaborate seamlessly. Together they iterate on the solution blueprint and a social experience that satisfies the stakeholder needs.

## SX tool suite

Solidground allows creators to focus on solutions that go far beyond traditional social media use cases. For this purpose Solidground offers a growing set of tools and methodologies where creators practice Social experience design with their clients. The basic idea is depicted in the [concept map](/explanation/domain-modeling/#concept-maps) diagram below.

<img src="/images/solidground-overview-sx_excalidraw-dark.png" class="dark-only"
  alt="Diagram that shows a concept map explaining how Solidground SX works. The creator involves the client to discover needs that must be satisfied by the solution to serve the stakeholders. A social experience tailored to these needs provides the solution. The creator follows the SX process and invokes recipes with automated steps that help iterate on the blueprint of the solution, consisting of configuration, documentation and code. A blueprint is exported as bundles that can be installed as service components on Groundwork server to host the solution."/>
<img src="/images/solidground-overview-sx_excalidraw-light.png" class="light-only"
  alt="Diagram that shows a concept map explaining how Solidground SX works. The creator involves the client to discover needs that must be satisfied by the solution to serve the stakeholders. A social experience tailored to these needs provides the solution. The creator follows the SX process and invokes recipes with automated steps that help iterate on the blueprint of the solution, consisting of configuration, documentation and code. A blueprint is exported as bundles that can be installed as service components on Groundwork server to host the solution."/>

The SX process works by invoking recipes that provide guidance of the steps to follow to execute particular tasks in the solution lifecycle. These steps can be documentation and checklists, but may also involve workflow automation of tasks and sub-tasks outlined in the recipe. As they go along they iterate on the blueprint of the solution using the Floorplanner design tools.

The [Weave SDK](/reference/weave-sdk) is invoked from recipe steps based on information provided in Floorplanner, or invoked directly on the command line by the creator. The SDK ensures that the blueprint of the solution becomes an integral part of the codebase, and helps keep specifications, documentation and tests in sync with the actual code as the solution evolves.

The blueprint packages configuration files, documentation and code into bundles for installation on a [Groundwork hosting](/guides/groundwork-hosting) server as service components that host the social experience.

## SX hosting

At any time a blueprint can be exported from Floorplanner in the form of a bundle that contains all artifacts of the solution: Bundles are self-contained archives of configuration, documents and code that can be opened and modified in any other Floorplanner design environment.

Bundles are installed as service components on a Groundwork host to offer social experiences in test and production environments. Service components are the building blocks of social experience design.

The event-driven architecture of Groundwork allows components to choreograph their behaviors together and interact in long-running workflows managed by [Taskweave engine](/guides/taskweave-engine). Service components are invoked with location transparency, interacting locally or with remote Groundwork hosts. Groundwork networking is agnostic to the protocol used and out-of-the-box supports communication with external services via the [W3C ActivityPub](https://www.w3.org/TR/activitypub/) open standard.

:::note[Component-oriented development]
Method to design tailored social experiences from fine-grained choreographies of more reusable building blocks.
:::

Well designed components in many cases will have uses beyond the original solution for which they were developed. Creators thus collect component libraries that will help their solution design productivity and an ecosystem of modules from many providers may emerge. Solidground envisions a paradigm of [App Free Computing](/explanation/component-development/#app-free-computing) that shifts the focus from siloed apps to delivery of flexible experiences.

## SX moldable methods

It is clear that SX covers a very large scope. Solidground out of the box offers an opinionated approach to solution design. However, each development process between a creator and a client has different unique requirements. For this purpose the Floorplanner designer can be customized to match individual preferences. Adjusting tools to the unique requirements and environment best suited for building a particular solution is called [Moldable development](https://moldabledevelopment.com/).

:::tip[Crowdsource SX best-practices]
Solidground does not attempt to define the detailed practices of Social experience design. Instead the tool suite provides SX practitioners the means to discover best-practices that work for them, and share them with others. Via our own social experience, the Floorplanner designer!
:::

No development process is the same. So Solidground offers [Moldable development methods](/explanation/moldable-methods) to your preferred SX process. The Weave SDK and Floorplanner designer together provide the automation framework to adjust the development method and process to optimally serve the creator. Floorplanner consists of service components and is highly extesible.

Solidground project will focus on community building and fostering of a growing open ecosystem where many third parties can offer their extensions and add value to your Social Web solutions.