---
title: Domain Modeling
description: How Strategic domain-driven design helps focus on people's Needs.
---

:::tip[Help improve the Docs]
Solidground is in early stages of development. Any [contribution](https://codeberg.org/solidground/housekeeping/src/branch/main/CONTRIBUTING.md) is welcome.
:::

[Domain Driven Design](https://en.wikipedia.org/wiki/Domain-driven_design) (DDD) is a well-known approach to software design that was introduced in ["the blue book"](https://www.dddcommunity.org/book/evans_2003/) by [Eric Evans](https://mastodon.social/@ericevans) in 2003.

The second part of the book deals with strategic design, and is much less well-known. It offers a comprehensive method to analyse the domains for which software solutions are created. This helps developers create the right abstractions in code, while keeping the people whom they build for closely involved. Strategic design helps focus on people's needs.

:::tip[Good to know]
Solidground focuses on domain-driven _strategic design_. Most search results on DDD relate to operational patterns and application to various architectures. Don't get distracted by these.
:::

Some form of strategic design _always_ happens. If it is not done explicitly it happens implicitly through the choices made by the developer, but in a way that is much harder to keep track of.

## Strategic design

Floorplanner makes strategic design explicit, and uses it to guide client and creator through an iterative process to evolve the solution, and ensures close collaboration along the way.

<img src="/images/solidground-domain-modeling-process_excalidraw-dark.png" class="dark-only"
  alt="Diagram that shows the domain modeling process in a cycle consisting of six connected stages. Each stage helps refine the ubiquitous language that is depicted in the center of the diagram. The stages are: 1. Identify problems and refine needs, 2. Explain with concept maps (optional), 3. Elaborate domains, define context maps, 4. Model events, UI mockups, CRUD forms, 5. Identify use cases, define behavior tests, 6. Scaffold the code, implement business logic. In the first three stages the client is in the lead, and in the last three stages the creator is."/>
<img src="/images/solidground-domain-modeling-process_excalidraw-light.png" class="light-only"
  alt="Diagram that shows the domain modeling process in a cycle consisting of six connected stages. Each stage helps refine the ubiquitous language that is depicted in the center of the diagram. The stages are: 1. Identify problems and refine needs, 2. Explain with concept maps (optional), 3. Elaborate domains, define context maps, 4. Model events, UI mockups, CRUD forms, 5. Identify use cases, define behavior tests, 6. Scaffold the code, implement business logic. In the first three stages the client is in the lead, and in the last three stages the creator is."/>

### Discover needs

The needs of the client and stakeholders are central in the social experience design method. Strategic design starts with discovery of the problems that must be addressed. In interaction and interviews with stakeholders the creator formulates the needs that must be satisfied by the solution. The findings in this early stage are both recorded in an informal textual format as markdown documents, and as Stakeholder needs based on a template.

<img src="/images/solidground-stakeholder-needs_excalidraw-dark.png" class="w-3/4 dark-only"
  alt="Diagram that shows two templates for capturing stakeholder + need + goal. The first template reads 'As a [Stakeholder role], I want to [Stakeholder need], but [Problem statement] and solution helps by [goal], unlike [existing prodedures/tools]'. The second template reads 'In order to [Stakeholder need], as a [Stakeholder role], I want to [have functionality], so that [goal]'."/>
<img src="/images/solidground-stakeholder-needs_excalidraw-light.png" class="w-3/4 light-only"
  alt="Diagram that shows two templates for capturing stakeholder + need + goal. The first template reads 'As a [Stakeholder role], I want to [Stakeholder need], but [Problem statement] and solution helps by [goal], unlike [existing prodedures/tools]'. The second template reads 'In order to [Stakeholder need], as a [Stakeholder role], I want to [have functionality], so that [goal]'."/>

Floorplanner does offer templates and guidance to get up to speed, and tracks stakeholders, identified needs and use cases in up-to-date lists. Needs are monitored throughout the solution design, to assure the social experience is tailored to match them appropriately.

### Ubiquitous language

The Ubiquitous Language (UL) is the terminology that all people involved in the solution design agree to use to reach a common understanding of the domain. It represents the domain concepts that are being modeled, and informs documentation, specifications, and code.

<img src="/images/solidground-ubiquitous-language_excalidraw-dark.png" class="dark-only"
  alt="Diagram that shows how client, creator and stakeholders contribute to the ubiquitous language, which is used in documentation, specifications and code, and anywhere else such as in whiteboarding sessions."/>
<img src="/images/solidground-ubiquitous-language_excalidraw-light.png" class="light-only"
  alt="Diagram that shows how client, creator and stakeholders contribute to the ubiquitous language, which is used in documentation, specifications and code, and anywhere else such as in whiteboarding sessions."/>

Using the same language consistently throughout the solution lifecycle significantly improves communication, and allows everyone to be on the same page. The UL helps ensure that non-technical stakeholders can stay in the loop throughout the development process, and allows them to timely verify that their needs will be satisfied by the solution.

Domain terminology consists of nouns and verbs that need to be well described. Floorplanner provides the functionality for creator and client to define the ubiquitous language of the solution, and makes it easy to keep it up to date as insights on the domain improve.

### Concept maps

Concept map diagrams may be created throughout the design process to help explain the relationships between domain concepts. A concept map is an informal model that is easy to draw and can help clarify particular aspects of the domain.

<img src="/images/solidground-concept-maps_excalidraw-dark.png" class="dark-only"
  alt="Diagram that shows a concept map that explains concept maps. Concept maps are 2-dimensional diagrams that provide a mental map of the relationships between domain concepts, to help with common understanding."/>
<img src="/images/solidground-concept-maps_excalidraw-light.png" class="light-only"
  alt="Diagram that shows a concept map that explains concept maps. Concept maps are 2-dimensional diagrams that provide a mental map of the relationships between domain concepts, to help with common understanding."/>

A concept map need not correspond to the technical implementation. However, once created, diagrams should be kept up-to-date in order to reflect the terminology used in the ubiquitous language. Concept maps are included in documentation artifacts created with Floorplanner.

### Bounded contexts

As needs are investigate and refined and more of the ubiquitous language is specified, different functional areas of the solution can be identified. These are separated into bounded contexts that split the top-level domain into separate sub-domains that are elaborated in isolation.

<img src="/images/solidground-context-map_excalidraw-dark.png" class="dark-only"
  alt="Diagram that shows the different components of a context map. The outer shape represents the top-level domain and contains three shapes: A core domain, a supportive domain, and a generic domain, connected by dotted lines."/>
<img src="/images/solidground-context-map_excalidraw-light.png" class="light-only"
  alt="Diagram that shows the different components of a context map. The outer shape represents the top-level domain and contains three shapes: A core domain, a supportive domain, and a generic domain, connected by dotted lines."/>

Core domains are most important and where most attention goes to. As an example, in Floorplanner the top-level domain is "Social experience design", a core domain is "Domain modeling" and it is supported by a "Scaffolding" domain that takes care of generation of artifacts for the solution blueprint. Another domain is "Diagramming", which is generic in nature and may be provided by an existing library or external service.

## Event modeling

Domains represent business processes where stakeholders interact. Event modeling focuses on the activities of the process and the information that is exchanged, and is an important step towards implementing the solution.

<img src="/images/solidground-event-modeling_excalidraw-dark.png" class="dark-only"
  alt="Diagram that shows the different components of a context map. The outer shape represents the top-level domain and contains three shapes: A core domain, a supportive domain, and a generic domain, connected by dotted lines."/>
<img src="/images/solidground-event-modeling_excalidraw-light.png" class="light-only"
  alt="Diagram that shows the different components of a context map. The outer shape represents the top-level domain and contains three shapes: A core domain, a supportive domain, and a generic domain, connected by dotted lines."/>

An event model is created in several stages: brainstorming, timelining, storyboarding and refinement. Deep involvement of client and stakeholders is key to the discovery of business processes to automate in the solution.

:::tip[Learn more about Event modeling]
For more information see: [Event Modeling: What is it?](https://eventmodeling.org/posts/what-is-event-modeling/)
:::

The following paragraphs describe the modeling stages that are conducted in parallel.

### Brainstorming

Elaboration starts with a brainstorming session. Client stakeholders helped by the creator place sticky notes describing activities that occur in the business process. The creator stimulates input of the participants, ensures that ubiquitous language is consistently used, and that new terminology is identified. The text on the sticky notes is formatted as events: things that happened in the past.

### Timelining

With guidance of the creator the sticky notes from the brainstorming session are placed on a timeline in the order in which they occur during the process. Visualising the business process on a timeline helps the client find missing activities and bring more detail. The creator helps improve the findings, removes duplicate sticky notes and irrelevant activities.

### Storyboarding

Based on the event timeline the creator and client commence with UX storyboarding. The timeline is enriched with rough sketches of UI widgets that help clarify the way in which stakeholders interact with the solution. The sketches need not be accurate UI as long as they help the stakeholders form a mental picture of the tasks they need to perform.

### Refinement

In the final stage of event modeling the creator adds more detail to the diagrams. UI widgets activate commands in the solution, which subsequently trigger events that lead to progression along the business process timeline. Before a widget can be displayed in the user interface it must receive its data from a view. The creator adds sticky notes for the various commands and views and connects them with arrows. The events on the timeline are placed in lanes that represent the bounded contexts in the context map.

In this refinement stage the creator is in the lead, and makes adjustments that are needed for the technical elaboration. In collaboration with stakeholders the data that is being exchanged throughout the event model is further elaborated with the help of Floorplanner functionality. As everywhere, the naming of data properties follows the ubiquitous language too.

With refinement done the domains are sufficiently elaborated and the resulting event models are ready for [Process design](/explanation/process-design) to take them further towards implementation in code.

