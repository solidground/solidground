---
title: Component Development
description: How component-oriented development facilitates versatile solutions that can evolve.
---

:::tip[Help improve the Docs]
Solidground is in early stages of development. Any [contribution](https://codeberg.org/solidground/housekeeping/src/branch/main/CONTRIBUTING.md) is welcome.
:::

## App free computing

Solidground moves beyond the App and envisions a Social Web of choreographed components and services.

### Components and services

<img src="/images/solidground-component-choreography-ui_excalidraw-dark.png" class="dark-only w-2/3"
  alt="Diagram that shows "/>
<img src="/images/solidground-component-choreography-ui_excalidraw-light.png" class="light-only w-2/3"
  alt="Diagram that shows "/>

Component-based development
Reusable components
Choreographed services


## Baseline architecture

### Actor model

Location transparency

### Event-based system

CQRS/ES architecture pattern
Choreography and orchestration

### Ports and adapters

Hexagonal architecture
ActivityPub support

### Service components