---
title: Process Design
description: How domain models are transformed into use case based process, ready for implementation.
---

## Event storming

<img src="/images/solidground-event-storming_excalidraw-dark.png" class="dark-only"
  alt="Diagram that shows "/>
<img src="/images/solidground-event-storming_excalidraw-light.png" class="light-only"
  alt="Diagram that shows "/>