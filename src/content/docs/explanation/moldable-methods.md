---
title: Moldable Methods
description: Learn how to adapt the development process of the Solidground tool suite itself.
---

## Development methods

The process guidance in Floorplanner is based on recipes that are intended to be improved to better serve the creators needs. In essence Floorplanner itself is a social experience offering a solution that should match the needs of creators, and continues to evolve. Kaizen, or continual improvement is a key focus area.

## Moldable development

## Recipes and steps