---
title: Weave SDK Reference
description: Reference documentation for the Weave software development kit.
---

## Preparation

Before using the SDK ensure that the programming environment is set up for your language. We recommend using [`asdf`](https://asdf-vm.com/) if you develop in multiple languages. When doing only front-end development, installation of the Rust or Golang SDK is still required to compile your service components.

Installation procedure:

1. Prepare your environment [for the Rust SDK](#prepare-rust-sdk) or [set up for Golang](#prepare-golang-sdk).
2. Set up [front-end development](#front-end-development) when developing UI components.

:::tip[Good to know]
The Rust SDK is most mature as Solidground project uses it as default when developing and testing new features. Please [report to our tracker](https://codeberg.org/solidground/solidground/issues/) if you encounter any issues.
:::

### Prepare Rust SDK

Install the Rust toolchain on your local computer, and preferred editor.

- Follow the instructions to [Install Rust](https://www.rust-lang.org/tools/install)
- When using `asdf` install the choolchain using the [asdf-rust](https://github.com/asdf-community/asdf-rust) plugin.

If you are using [VSCodium](https://vscodium.com/) or VSCode, the following plugins are recommended:

- [Rust analyzer plugin](https://open-vsx.org/extension/rust-lang/rust-analyzer)

### Prepare Golang SDK

:::tip[Interested in Golang support?]
Can't wait to get started with Golang? Help speed up our development and contribute to the Weave Golang SDK.
:::

### Front-end development

For [VSCodium](https://vscodium.com/) or VSCode, the following plugins are recommended:

- [Astro support plugin](https://open-vsx.org/extension/astro-build/astro-vscode)
- [Tailwind CSS IntelliSense plugin](https://open-vsx.org/extension/bradlc/vscode-tailwindcss)
- [MDX support plugin](https://open-vsx.org/extension/unifiedjs/vscode-mdx)