---
title: Glossary
description: Common language that we use throughout the Solidground Project.
---

This Glossary represents the [_ubiquitous language_](/explanation/domain-modeling/#ubiquitous-language) terminology used throughout Solidground project.

:::caution[Keep the glossary up-to-date]
The ubiquitous language must be accurate and actual. If you find concepts that are missing, then please propose them in our channels or file an issue to our [documentation tracker](https://codeberg.org/solidground/documentation/issues).
:::

## Solidground Tool Suite

This section contains terminology that is specific to the Solidground tool suite. But first ..

:::caution[Words we do not use]
Solidground is a new type of software, called a Moldable operational system. These familiar terms do not apply and must be avoided in project communication and documentation:

- _**App, Application**_ - We move beyond the restrictive concept of "applications" where functionality is packaged in particular ways that form siloes and collaboration barriers. Use terms like "solution", "system", "software" or "social experience" instead.
- _**Cloud**_ - The "cloud" has become synonymous for corporate vendor lock-in in commercial SaaS products. Solidground favors open ecosystems and explores a new paradigm of moldable operational systems on a decentralized Social Web.
- _**Customer**_ - Solidground focuses on sustainable Free Software, which may include paid services. Yet Free Software is for everyone. People's needs are central. We prefer the term "clients" instead of "customers".
- _**Platform**_ - Platforms indicate siloed functionality. Web 2.0 is the world of cloud-based "platforms", which has no meaning in Solidground. Solutions consist of modular components and services that are available on the Social Web.
- _**Product**_ - Considering Solidground as a "product" doesn't match the vision of collaboration on the Social Web where social networks integrate many solutions. We do "solution development" and try to leverage the ecosystem.
:::

### Workspace management

| Term | Explanation |
| :---: | :--- |
| Client | Stakeholder group. Persons that need the social experience. The domain experts that want to see their needs addressed in features and requirements implemented in the solution. |
| Collaboration Hub | |
| Creator | Stakeholder group. Any person involved in creating and evolving social experiences for the Fediverse. This group has people in many roles, like Developer, Designer, Tester, etc. |
| Design Project | |
| Design Workspace | Collection of projects for a particular client. |
| Project Discipline | A parallel project track that consists of a related set of activities, procedures and tasks that occur on an ongoing basis throughout the solution lifecycle. |
| Project Phase | A discreet stage of the solution lifecycle. |

### Social experience design

The top-level business domain of the Solidground project. Contains all concepts related to the solution design process followed by client and creator to deliver a social experience for stakeholders.

| Term | Explanation |
| :---: | :--- |
| Blueprint | Artifacts that comprise a social experience being created with the Solidground tool suite. |
| Bundle | Blueprint that is packaged in a single compressed file for download or transfer to other locations. |
| Development Method | The entirety of development practices that are in use by a creator. They include all development processes, tools used, and stakeholders involved. |
| Development Process | Specific part of the development method that involve workflows to help coordinate manual and automated activities. |
| Domain modeling | |
| Needs | Requirements and objectives of stakeholders that must be taken into account during the SX process. |
| Playbook | Pre-defined sequence of recipes and steps in a SX process to provide guidance for particular project stages or disciplines. |
| SX Process | The methodolical approach to SX that comprises all activities that occur during the solution lifecycle. |
| Process Guidance | Help with the SX process in the form of structured documentation, tool support and automation to perform tasks and workflows according to best-practices. Process guidance is provided by playbooks and recipes. |
| Recipe | A procedure or guideline that outlines process Steps that lead to improvements of the solution blueprint. |
| Scaffolding | Generate project structure and code from information gathered in a Floorplanner Blueprint design. |
| Service Component | A packaged part of the social experience that can be deployed on Groundwork. Usually it contains the functionality that relates to a discreet bounded context of the domain design. |
| Social Experience | Social networking solution created by the Solidground tool suite tailored to stakeholder needs, that can be hosted as service components on the Groundwork platform. |
| Social Experience Design | Acronym SX. Methodical and inclusive development method to creating collaborative social networking solutions that match all stakeholder Needs. |
| Solution | The final deliverable defined by the blueprint that satisfies the client's Needs. |
| Solution Lifecycle | Encompasses all activities of solution design from first inception to phasing out, end of life. |
| Stakeholder | Any person or group of people directly or indirectly involved in Social Experience Design, whose needs must be taken into account. |
| Step | Documented artifact outlining a single manual or automated task, activity or procedure that is included in a recipe. Steps are executable in workflow automation provided by Taskweave Engine. |
| Use Case | |

### Domain modeling

| Term | Explanation |
| :---: | :--- |
| Bounded Context | Distinct area of the solution that models closely related concepts and activities in a way that assures business processes are always consistent. |
| Business Process | Structured activities that stakeholders perform in their daily work with help of the social experience being designed. |
| Concept Map | Diagram of domain concepts and their relationships that helps clarify specific aspects of the domain or a business process. |
| Context Map | Diagram that depicts the identified bounded contexts and their relationships. |
| Domain Driven Design | Acronym DDD. A structural approach to analyse client needs and keep them in focus throughout the entire development lifecycle. |
| Event Modeling | Method to gradually elaborate domains into event models in a way where the client and stakeholders are deeply involved. |
| Storyboarding | Event modeling phase where rough UI sketches are added to the event timeline. |
| Timelining | Event modeling phase where brainstormed events are placed in order of occurrence on a timeline. |
| Refinement | Event modeling phase where the creator brings more detail to the diagrams, to help find gaps in the business process and ease transition to implementation of the models. |
| Ubiquitous Language | The terminology that all stakeholders agree to use consistently when referring to concepts of a particular domain. |

### Process design

| Term | Explanation |
| :---: | :--- |
| Event Storming | Further refinement by the creator of the event models such that they can be readily implemented per use case into the solution design. |

### Joyful creation

Joyful creation isn't a domain that is modeled in our tools, but a paradigm of inclusive solution development that has its own terminology. For more background, please visit [Social Coding Movement](https://coding.social).

| Term | Explanation |
| :---: | :--- |
| FSDL | Free Software Development Lifecycle. Comprising all activities and processes that are relevant to creating sustainable Free Software projects. The lifecycle encompasses all activities, from the project's inception to its end-of-life. |
| Moldable Development | |
| MOS | Moldable Operational System. |
| Social Coding | An inclusive approach to Free Software development that focuses on people's needs and fostering fostering of healthy projects, communities and ecosystems. |

## Common terminology

### Software Development

| Term | Explanation |
| :---: | :--- |
| EDA | Event Driven Architecture. A type of software architecture where event messages are exchanged between the various components of the architecture, usually over an event bus. |
| FOSS | Free and Open Source Software. This is equivalent to saying 'Free Software'. Free as in "Freedom", not "Gratis" and indicated by its license. We recognize approved licenses of Open Source Initiative (OSI) and Free Software Foundation (FSF). |
| Kaizen | A mindset that focuses on continual improvement and finding small ways to make things better. |

### Architecture

| Term | Explanation |
| :---: | :--- |
| Aggregate Root | Concept of Event Sourcing pattern. Code construct that represents the main concept and consistency boundary of a bounded context. The aggregate encapsulates domain logic and only consumes commands and emits domain events. The aggregate forms the domain layer that is independent of service and infrastructure layers by inversion of control. |
| CQRS | Command Query Responsibility Segregation. Architecture pattern that separates write-side, where commands mutate data, from read-side of the architecture, where queries retrieve data. |
| Event Sourcing | Architecture pattern where domain events are used to hydrate the state of aggregates at any moment in time. In an event-sourced solution no data is overwritten as happens in CRUD applications. All state mutations are stored as event in an event store. |
| Read Model | Concept of CQRS pattern. A data model that is optimal to be queried by a user interface. Read models are updated by domain events, and often represent denormalized views of a domain model that avoid database overhead. |

### Fediverse

| Term | Explanation |
| :---: | :--- |
| FEP | Fediverse Enhancement Process. A process defined by the SocialHub community to specify extensions to the ActivityPub protocol. |
