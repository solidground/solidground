---
title: Solidground Requirements
description: An overview of major requirements that steer the direction of the Solidground Project.
---

:::tip[Help improve the Docs]
Solidground is in early stages of development. Any [contribution](https://codeberg.org/solidground/housekeeping/src/branch/main/CONTRIBUTING.md) is welcome.
:::

## Functional requirements

### Moldable methods

#### Process driven design

#### Component oriented development

## Non functional requirements

### Inclusive

### Accessible

### Internationalized

### Free software

## Techstack

## Toolstack