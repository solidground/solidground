---
title: Groundwork Labs
description: An overview of areas of Research & Innovation to be taken on in the Solidground ecosystem.
---

:::tip[Join us on Groundwork Labs]
Our research interests cover broad areas in both technical and social fields. We can't innovate alone, and want everyone to be able to explore with us. Put your creativity and abilities to best use, and together we will unleash the full potential of the Social Web.

For this purpose we intend to launch [Groundwork Labs](https://labs.solidground.work) that operates independently of the Solidground project as a community governed research and innovation center.
:::

## Social experience design

- Field exploration, crowdsourcing
- Best practice pattern library
- Inclusion & diversity in FOSS

## App free computing

- Process orientation
- Component-based development
- Distributed service choreography

## Moldable operational systems

- Moldable development
- Malleable systems
- Online collaboration

## Social web

- Fediverse protocols & interoperability
- Small world networking
- Personal social networking