---
title: Contribute to Solidground
description: No matter your skills, you can help make Solidground better. Together we innovate!
---

## Social coding

## Participate now

### Your feedback counts

### Help spread the word

### Improve the docs

### Test-drive our solutions

### Design the experience

### Develop the code

### Research our future