---
title: Designing Floorplanner Blueprints
description: How to design a social experience blueprint with Floorplanner.
---

:::tip[Help improve the Docs]
Solidground is in early stages of development. Any [contribution](https://codeberg.org/solidground/housekeeping/src/branch/main/CONTRIBUTING.md) is welcome.
:::

## Design Workspace