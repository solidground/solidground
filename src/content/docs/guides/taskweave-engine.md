---
title: Taskweave development kit
description: How-to use the taskweave command-line tools and SDK.
---

:::tip[Help improve the Docs]
Solidground is in early stages of development. Any [contribution](https://codeberg.org/solidground/housekeeping/src/branch/main/CONTRIBUTING.md) is welcome.
:::

## Working with the Weave CLI

## Bootstrap a Design Project