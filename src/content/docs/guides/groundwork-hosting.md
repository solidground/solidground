---
title: Groundwork Hosting
description: Guide to the Groundwork moldable operational system.
---

:::tip[Help improve the Docs]
Solidground is in early stages of development. Any [contribution](https://codeberg.org/solidground/housekeeping/src/branch/main/CONTRIBUTING.md) is welcome.
:::

## Work environments

<img src="/images/solidground-deployment-options_excalidraw-dark.png" class="dark-only"
  alt="Diagram that shows"/>
<img src="/images/solidground-deployment-options_excalidraw-light.png" class="light-only"
  alt="Diagram that shows"/>

## Solidground design hub

## Self-hosted collaboration hub

## Moldable operational system

<div class="header-section">
  <img src="/images/solidground-moldable-operational-system_excalidraw-dark.png" class="dark-only"
    alt="Diagram that shows a conceptual overview of the Solidground tool suite. On the right are three blocks.
      From top to bottom they are Floorplanner social experience for experience design, Taskweave workflow engine
      to support moldable processes, and separated by a dotted line an example of a system integration with a
      code forge. On the right side a block depicts the Solution containing checklists for Needs, Use cases
      and Roadmap plans"/>
  <img src="/images/solidground-moldable-operational-system_excalidraw-light.png" class="light-only"
    alt="Diagram that shows a conceptual overview of the Solidground tool suite. On the right are three blocks.
    From top to bottom they are Floorplanner social experience for experience design, Taskweave workflow engine
    to support moldable processes, and separated by a dotted line an example of a system integration with a
    code forge. On the right side a block depicts the Solution containing checklists for Needs, Use cases
    and Roadmap plans"/>
</div>