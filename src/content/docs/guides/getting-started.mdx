---
title: Start your experience
description: Introduction to Solidground tool suite and Social experience design.
---

import { Tabs, TabItem } from '@astrojs/starlight/components';

:::caution[Early stages of development]
Solidground project just started. We create our own social experience, to allow others to craft social experiences in turn. Solidground constitutes a new type of software, a [moldable operational system](/guides/groundwork-hosting/#moldable-operational-system). Our research and innovation explores new paradigms of computing.

This integrated _living documentation_ is part of that exploration. The docs often refer to functionality that hasn't been build, but serves to drive our design decisions. Things are bound to change frequently as we evolve our solution. No matter your skills, you can help us improve. We encourage you to [come along with us](/development/contributing) on our quest. **Together We Reimagine Social**.
:::

Welcome to Solidground Docs. Let's get started with the Solidground tool suite.

## Introduction

Solidground helps ease the design and creation of solutions for the Social Web. Solidground offers _Social experience design_ (SX), an inclusive method that guides a client and creator through a highly collaborative process. The main focus is on _needs-based evolution_ where the needs of all stakeholders are central throughout the entire solution lifecycle.

Before delving in, it is advisable to read more background on [Social experience design](/explanation/social-experience-design) and how Solidground helps you practice it. Out of the box the Solidground tool suite provides an opinionated set of practices to solution design. We recommend to start with the default tool configuration and gradually customize the [development method](/explanation/moldable-methods/#development-methods) to personal preferences.

### Client and creator

The documentation uses the terminology of "client" and "creator" to divide into groups of those who need a solution, the "domain experts" and often non-technical people, and those who build it, the technical people in IT roles. Both of these groups are stakeholders that have needs with regards to the solution, that must be taken into account. Solidground considers the client and creator to be part of the same social network for the duration of the solution lifecycle.

Clients and creators are fully independent parties. A client can use the services of multiple creators, and creators can serve multiple clients. The Floorplanner [Design Workspace](/guides/floorplanner-designer/#design-workspace) keeps track of all ongoing projects and the relationships between clients and creators.

### Solidground tools

Three key tools provide the highly integrated development environment. These are:

- [Groundwork Hosting](/guides/groundwork-hosting): Social network infrastructure to integrate and host social experiences.
- [Floorplanner Designer](/guides/floorplanner-designer): Online collaboration for client and creator to iterate on solutions.
- [Taskweave Engine](/guides/taskweave-engine): Workflow manager that facilitates both solution and tool processes.

In addition the creator has two additional productivity tools to their disposal:

- [Weave CLI](/reference/weave-cli): Command-line interface provides direct access to much of the functionality.
- [Weave SDK](/reference/weave-sdk): Software development kit to bootstrap solution design in supported languages.

Working with Solidground involves setting up a local work environment and connecting to a collaboration hub.

## Local tool set up

The client and creator can choose different [work environments](/guides/groundwork-hosting/#work-environments), adapted to their respective skill sets. The client typically only needs the design environment provided by Floorplanner Designer, while creators download the complete Solidground monorepo containing the entire tool suite as well as the source code to develop against.

:::tip[Good to know]
We intend to offer easier installation options for clients. Clients with technical skills may also opt to work with the full development environment.
:::

### Design environment

The client has two options and can opt for local-first or web-based experience design.

#### Local-first design

Local-first design offers the best experience, and requires installation of an executable on the local computer. To start, download the release package to your computer, unpack the archive to a directory of choice, and run `weave start`.

{/* TODO: Create component that inserts latest stable version number at designated locations. */}
The latest stable release of Solidground design environment is: [`v0.1.0`](https://codeberg.org/solidground/solidground/releases/tag/v0.1.0)

<Tabs>
	<TabItem label="Linux">
    > Create a directory "solidground" where you want to install, and navigate to it. Then find the latest Floorplanner Designer version in the [Releases](https://codeberg.org/solidground/solidground/releases) list for your architecture e.g. `floorplanner-designer-v0.1.0-linux-amd64.tar.gz` and click in the UI to download and use `tar -xz` to unpack the archive. Alternatively you can use `wget` from the command line (adapt to your preference):
    >
    > ```sh
    > wget -c http://codeberg.org/solidground/solidground/releases/downloads/floorplanner-designer-v0.1.0-linux-amd64.tar.gz -O - | tar -xz
    > ```
    > Now you can start the designer by running `./floorplanner` from the "solidground" directory.
  </TabItem>
	<TabItem label="MacOS">
  </TabItem>
  <TabItem label="Windows">
  </TabItem>
</Tabs>

Congratulations. You are now ready to sign up to a collaboration hub and create your [Design Workspace](/guides/floorplanner-designer/#design-workspace). To do so just follow the onboarding procedure in the Designer and either select the [Solidground Design Hub](https://design.solidground.work), or fill in the URL of your self-hosted collaboration hub, or one offered by a hosting provider.

#### Web-based design

For web-based experience design a regular web browser like Firefox, Chrome or Safari is sufficient. The address to navigate to depends on client preference for hosting their workspace.

<Tabs>
	<TabItem label="Solidground Hub">
    > Easiest option is to use the Solidground Design Hub, our own relay server that facilitates collaboration between clients and creators. Simply go to [`design.solidground.work`](https://design.solidground.work) and sign up. After the onboarding procedure your Design Workspace is ready to go.
  </TabItem>
  <TabItem label="Hosting provider">
    > You may also opt for a collaboration hub of a 3-rd party provider. This may be the creator you work with, allowing you to sign up and who may also administer your design workspace for you and your stakeholders. Any self-hosted hub can function as hosting provider.
  </TabItem>
	<TabItem label="Self-hosted">
    > A collaboration hub can be hosted on premise or at an internet service provider. The client has full control of the server. The installation procedure requires some technical skills, and the client is responsible for maintenance tasks. See [self-hosting a hub](#self-hosted-collaboration-hub) for installation procedure.
  </TabItem>
</Tabs>

### Development environment

The installation of Solidground for development is straightforward. The git repository of the Solidground project contains everything that is required. To get started, first [prepare your local development environment](/reference/weave-sdk/#preparation), then clone the full `solidground` repository to the local computer and launch Floorplanner Designer using the [`weave`](/reference/weave-cli) command-line interface:

```sh
git clone git@codeberg.org:solidground/solidground.git
cd solidground
weave start
```

Upon first use `weave` will run an interactive setup process. When done, the Floorplanner Designer launches in the [Tauri toolkit](https://tauri.app/) and runs in developer mode. This installation procedure is enough to start collaborating with clients in early stages of the SX process that are supported by [Floorplanner](/guides/floorplanner-designer).

Some further steps are required in order to develop the code for a client solution. They involve setting up programming language support. There are two options to choose from:

## Collaboration hubs

A collaboration hub is a headless installation of [Groundwork Hosting](/guides/groundwork-hosting) on a server. Hubs are part of the Social Web and can federate with each other. At least one collaboration hub is required to facilitate the collaboration between a client and a creator.

### Solidground Design Hub

Solidground hosts their own collaboration hub at [`design.solidground.work`](https://design.solidground.work) to help facilitate matchmaking and collaboration between clients and servers. The hub hosts the [Floorplanner](/guides/floorplanner-designer) social experience, which can be used for [web-based design](#web-based-design) by clients.

:::tip[Good to know]
Your designs are end-to-end encrypted so Solidground Design Hub will never get to see them. See the [Privacy Policy](https://design.solidground.work/privacy) and [Terms of Service](https://design.solidground.work/terms) for more information.
:::

The Solidground Design Hub makes it easy to get started with your first experience designs. At any time it is possible to migrate your work to a collaboration hub that is self-hosted or offered by a 3rd-party hosting provider on the Social Web.

### Self-hosted collaboration hub

We find the ability to self-host the entire Solidground tool suite to be important for a healthy ecosystem. After all the Social Web is inherently decentralized. Self-hosting is a need of both clients and creators, that we recognize. See the [Groundwork Hosting](/guides/groundwork-hosting/#self-hosted-collaboration-hub) guide for detailed instructions.

