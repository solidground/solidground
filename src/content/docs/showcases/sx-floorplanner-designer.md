---
title: Floorplanner Designer
description: This showcase elaborates the social experience design for Solidground Floorplanner itself.
---

:::tip[Good to know]
Here we elaborate the social experience of Floorplanner itself in an extreme case of [eating our own cooking](https://en.wikipedia.org/wiki/Eating_your_own_dog_food#Alternative_terms). Shortcuts will be taken, for the simple reason we still lack our own tools.
:::