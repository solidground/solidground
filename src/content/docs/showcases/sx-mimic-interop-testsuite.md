---
title: Mimic Interoperability Suite
description: This showcase presents a social experience design for interopability testing on the Fediverse.
---

:::tip[Good to know]
Mimic Interoperability Suite is a social experience design to be elaborated once Solidground reaches a higher level of maturity. For now see [SocialHub Testing](https://socialhub.activitypub.rocks/c/activitypub/testing/77) for Fediverse test suites.
:::

<img src="/images/solidground-sx-showcase_mimic-interop-suite-website.png" alt="Screenshot of the Mimic landing page at mimic.coding.social"/>